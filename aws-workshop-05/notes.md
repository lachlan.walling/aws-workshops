Continuous integration, deployment
1. Code commit (VCS)
2. Code build (Integration + deployment)
3. Code pipeline (integration + deployment++)

- Each service publishes events to cloudwatch.
- Codebuild publishes events for branches being updated
- Codebuild can respond to event, start a build, and publish to s3 bucket

Code commit -> event bus -> code build -> s3

- Event may need to be translated (in event bus)
  - Code commit event -> code build event
