#!/bin/bash
set -eo pipefail

queryOutput() {
  local key="$1"
  aws cloudformation describe-stacks \
    --stack-name bit-by-bit \
    --query "Stacks[0].Outputs[?OutputKey=='$key'].OutputValue | [0]" \
    --output text
}

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <profile>"
  exit 1
fi

export AWS_PROFILE="$1"

BUCKET_NAME=$(queryOutput BucketName)
aws s3 rm "s3://$BUCKET_NAME" --recursive

aws cloudformation delete-stack --stack-name bit-by-bit
aws cloudformation wait stack-delete-complete --stack-name bit-by-bit