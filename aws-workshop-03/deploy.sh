#!/bin/bash
set -eo pipefail

queryOutput() {
  local key="$1"
  aws cloudformation describe-stacks \
    --stack-name bit-by-bit \
    --query "Stacks[0].Outputs[?OutputKey=='$key'].OutputValue | [0]" \
    --output text
}

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <profile> <directory to upload>"
  exit 1
fi

export AWS_PROFILE="$1"
SRC_PATH="$2"

aws cloudformation deploy --stack-name bit-by-bit --template-file ./bit-by-bit.yaml

BUCKET_NAME=$(queryOutput BucketName)
aws s3 sync "$SRC_PATH" "s3://${BUCKET_NAME}"

echo "Distribution: https://$(queryOutput DistributionDomainName)"