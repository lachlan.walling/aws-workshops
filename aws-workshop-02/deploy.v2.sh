#!/bin/bash
set -e -o pipefail

generateBucketName() {
  uuidgen
}

createBucket() {
  checkProfile

  local name="$(generateBucketName)"
  if aws s3 mb "s3://${name}" > /dev/null; then 
      useBucket "$name"
  fi
}

allowCloudfrontToAccessBucket() {
  checkProfile
  checkBucket
  local originAccessIdentity="$1"

  local policyFile=$(mktemp /tmp/bucket-policy.XXXXXX.json)
  
  cat <<-EOF > ${policyFile}
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
          "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity $originAccessIdentity"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::$BUCKET_NAME/*"
    }
  ]
}
EOF
  aws s3api put-bucket-policy --bucket "${BUCKET_NAME}" --policy "file://${policyFile}"

  rm "${policyFile}"
}


uploadToBucket() {
  local srcPath="$1"

  if [ -z "$srcPath" ]; then
    writeError "Usage: uploadToBucket <path>"
    return 1
  fi

  checkProfile
  checkBucket
  aws s3 sync "$srcPath" "s3://${BUCKET_NAME}"
}

createCachePolicy() {
  checkProfile
  checkBucket
  local configFile=$(mktemp /tmp/cache-config.XXXXXX.json)
  cat <<-EOF > ${configFile}
{
  "Comment": "Cache policy for $BUCKET_NAME",
  "Name": "$(uuidgen)",
  "MinTTL": 86400
}
EOF

  aws cloudfront create-cache-policy \
    --cache-policy-config "file://$configFile" \
    --query CachePolicy.Id \
    --output text
  
}

createDistribution(){
  checkProfile
  local originAccessIdentity="$1"
  local cachePolicyId="$2"

  local configFile=$(mktemp /tmp/distribution-config.XXXXXX.json)

  local success=0
  cat <<-EOF > ${configFile}
{
  "CallerReference": "$BUCKET_NAME",
  "Comment": "Serves content from $BUCKET_NAME",
  "Enabled": true,
  "DefaultRootObject": "index.html",
  "Origins": {
    "Quantity": 1,
    "Items": [
      {
        "Id": "$BUCKET_NAME",
        "DomainName": "$(getBucketDomain)",
        "S3OriginConfig": {
          "OriginAccessIdentity": "origin-access-identity/cloudfront/$originAccessIdentity"
        }
      }
    ]
  },
  "DefaultCacheBehavior": {
    "TargetOriginId": "$BUCKET_NAME",
    "ViewerProtocolPolicy": "redirect-to-https",
    "CachePolicyId": "$cachePolicyId"
  }
}
EOF
  local id=$(aws cloudfront create-distribution \
    --distribution-config "file://$configFile" \
    | jq -r .Distribution.Id)
  useDistribution "$id"
  waitForDistributionOperation "create"
}

createOriginAccessIdentity() {
  checkProfile
  checkBucket
  aws cloudfront create-cloud-front-origin-access-identity \
    --cloud-front-origin-access-identity-config \
    CallerReference="${BUCKET_NAME}",Comment="Origin access identity" \
    --query CloudFrontOriginAccessIdentity.Id \
    --output text
  sleep 10
}

deploy() {
    local srcPath="$1"
    if [ -z "$srcPath" ]; then
      writeError "Usage: deploy <pathToUpload>"
      return 1
    fi
    createBucket
    uploadToBucket "$srcPath"
    echo "Bucket name: $BUCKET_NAME"
    echo "Bucket domain: $(getBucketDomain)"

    local originAccessIdentity=$(createOriginAccessIdentity)
    echo "Origin access identity id: $originAccessIdentity"
    local cachePolicyId=$(createCachePolicy)
    echo "Cache policy id: $cachePolicyId"

    allowCloudfrontToAccessBucket "$originAccessIdentity"

    createDistribution "$originAccessIdentity" "$cachePolicyId"
    echo "Distribution id: $DISTRIBUTION_ID"
    echo "Distribution: https://$(getDistributionDomain)"
}

SCRIPT_LOCATION=$(dirname -- "$0")
source "${SCRIPT_LOCATION}/common.sh"

if [ "$#" -ne 2 ]; then
  writeError "Usage $0 <profile> <path to upload>"
  exit 1
fi

PROFILE="$1"
SRC_PATH="$2"

useProfile "$PROFILE"
deploy "$SRC_PATH"