#!/bin/bash
set -e -o pipefail

deleteBucket() {
    checkProfile
    checkBucket

    aws s3 rb --force "s3://${BUCKET_NAME}"
}

deleteDistribution() {
    checkProfile
    checkDistribution

    disableDistribution

    local etag="$(getDistributionETag)"

    aws cloudfront delete-distribution \
      --id "$DISTRIBUTION_ID" \
      --if-match "$etag"
}

deleteOriginAccessIdentity() {
  checkProfile
  local originAccessIdentityId="$1"

  local etag=$(aws cloudfront get-cloud-front-origin-access-identity \
    --id "$originAccessIdentityId" \
    --query ETag \
    --output text)

  aws cloudfront delete-cloud-front-origin-access-identity \
    --id "$originAccessIdentityId" \
    --if-match "$etag"
}

deleteCachePolicy(){
  checkProfile
  local cachePolicyId="$1"
  local etag=$(aws cloudfront get-cache-policy \
    --id "$cachePolicyId" \
    --query ETag \
    --output text)

  aws cloudfront delete-cache-policy \
    --id "$cachePolicyId" \
    --if-match "$etag"
}

disableDistribution() {
  checkProfile
  checkDistribution

  local etag=$(getDistributionETag)

  local configFile=$(mktemp /tmp/cloudfront-config.XXXXXX.json)

  local success=0
  getDistributionConfig | jq .Enabled=false > ${configFile} &&
  aws cloudfront update-distribution \
      --id "$DISTRIBUTION_ID" \
      --if-match "$etag" \
      --distribution-config file://"$configFile" \
      > /dev/null &&
  waitForDistributionOperation "disable" || {
    success=$?
  }

  rm "$configFile"
  return $success
}

destroy() {
    deleteDistribution
    deleteBucket
}

SCRIPT_LOCATION=$(dirname -- "$0")
source "${SCRIPT_LOCATION}/common.sh"

if [ "$#" -ne 5 ]; then
  writeError "Usage $0 <profile> <bucket name> <distribution id> <cache policy id> <origin access identity id>"
  exit 1
fi

PROFILE="$1"
BUCKET_NAME="$2"
DISTRIBUTION_ID="$3"
CACHE_POLICY_ID="$4"
ORIGIN_ACCESS_IDENTITY="$5"

useProfile "$PROFILE"
useBucket "$BUCKET_NAME"
useDistribution "$DISTRIBUTION_ID"
destroy
deleteCachePolicy "$CACHE_POLICY_ID"
deleteOriginAccessIdentity "$ORIGIN_ACCESS_IDENTITY"