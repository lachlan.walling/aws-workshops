#!/bin/bash
set -e -o pipefail

generateBucketName() {
    uuidgen
}

createBucket() {
    checkProfile

    local name="$(generateBucketName)"
    if aws s3 mb "s3://${name}" > /dev/null; then 
        useBucket "$name"
    fi
}

removeBucketPublicBlock() {
    checkProfile
    checkBucket
    aws s3api delete-public-access-block --bucket "${BUCKET_NAME}"
}

makeBucketPublic() {
    checkProfile
    checkBucket

    local policyFile=$(mktemp /tmp/bucket-policy.XXXXXX.json)

    
    local success=0
    cat <<-EOF > ${policyFile} &&
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${BUCKET_NAME}/*"
            ]
        }
    ]
}
EOF
    aws s3api put-bucket-policy --bucket "${BUCKET_NAME}" --policy "file://${policyFile}" || {
      success=$?
    }
    rm -rf "${policyFile}"
    return $success
}


uploadToBucket() {
    local srcPath="$1"

    if [ -z "$srcPath" ]; then
        writeError "Usage: uploadToBucket <path>"
        return 1
    fi

    checkProfile
    checkBucket
    aws s3 sync "$srcPath" "s3://${BUCKET_NAME}"
}

setBucketWebsite() {
    checkProfile
    checkBucket
    aws s3 website "s3://${BUCKET_NAME}" --index-document index.html
}

createDistribution(){
    checkProfile
    local id=$(aws cloudfront create-distribution \
        --origin-domain-name "$(getBucketWebsiteDomain)" \
        --default-root-object index.html | jq -r .Distribution.Id)
    useDistribution "$id"
    waitForDistributionOperation "create"
}

deploy() {
    local srcPath="$1"
    if [ -z "$srcPath" ]; then
        writeError "Usage: deploy <pathToUpload>"
        return 1
    fi
    createBucket
    removeBucketPublicBlock
    makeBucketPublic
    setBucketWebsite
    uploadToBucket "$srcPath"
    echo "Bucket name: $BUCKET_NAME"
    echo "Bucket url: http://$(getBucketDomain)"
    createDistribution
    echo "Distribution id: $DISTRIBUTION_ID"
    echo "Distribution: https://$(getDistributionDomain)"
}

SCRIPT_LOCATION=$(dirname -- "$0")
source "${SCRIPT_LOCATION}/common.sh"

if [ "$#" -ne 2 ]; then
  writeError "Usage $0 <profile> <path to upload>"
  exit 1
fi

PROFILE="$1"
SRC_PATH="$2"

useProfile "$PROFILE"
deploy "$SRC_PATH"