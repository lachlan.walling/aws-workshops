#!/bin/bash

writeError() {
    echo "$1" 1>&2
}

useProfile() {
    export AWS_PROFILE="$1"
}

useBucket() {
    BUCKET_NAME="$1"
}

useDistribution(){
    DISTRIBUTION_ID="$1"
}

checkProfile() {
    if [ -z "$AWS_PROFILE" ]; then
        writeError "Profile not set"
        return 1
    fi
}

checkBucket() {
    if [ -z "$BUCKET_NAME" ]; then
        writeError "Bucket not set"
        return 1
    fi
}

checkDistribution() {
    if [ -z "$DISTRIBUTION_ID" ]; then 
        writeError "Distribution not set"
        return 1
    fi
}

getBucketDomain() {
    checkBucket
    echo "${BUCKET_NAME}.s3.ap-southeast-2.amazonaws.com"
}

getBucketWebsiteDomain() {
    checkBucket
    echo "${BUCKET_NAME}.s3-website.ap-southeast-2.amazonaws.com"
}

queryDistribution() {
    local query="$1"
    checkProfile
    checkDistribution
    aws cloudfront get-distribution \
        --id "$DISTRIBUTION_ID" \
        --output text \
        --query "$query"
}

getDistributionStatus() {
    queryDistribution Distribution.Status
}

getDistributionDomain() {
    queryDistribution Distribution.DomainName
}

getDistributionETag() {
    queryDistribution ETag
}

isDistributionInProgress() {
    local status="$(getDistributionStatus)"
    [ "$status" == "InProgress" ]
}

waitForDistributionOperation() {
    local action="$1"
    echo "Waiting for distribution to $action"
    aws cloudfront wait distribution-deployed --id $DISTRIBUTION_ID
}

getDistributionConfig() {
    checkProfile
    checkDistribution
    aws cloudfront get-distribution-config --id "$DISTRIBUTION_ID" --query DistributionConfig
}