#!/bin/bash
set -eo pipefail

export AWS_PROFILE="j1-pf-sandbox-admin"

if [ -z $1 ]; then
  STACK_NAME='pipeline'
  BACKEND_STACK_NAME='pipeline-backend'
else
  STACK_NAME="$1"
  BACKEND_STACK_NAME="$1-backend"
fi

echo Deploying frontend pipeline
aws cloudformation deploy --template-file ./pipeline.yml --stack-name $STACK_NAME --capabilities CAPABILITY_NAMED_IAM

echo Deploying backend pipeline
aws cloudformation deploy --template-file ./pipeline-backend.yml --stack-name $BACKEND_STACK_NAME --capabilities CAPABILITY_NAMED_IAM
