const {S3} = require('@aws-sdk/client-s3');
const process = require('process');
const response = require('cfn-response');

async function emptyBucket(bucketName) {
  const s3 = new S3()
  let objects = (await s3.listObjectsV2({Bucket: bucketName})).Contents || []
  if (objects.length > 0) {
    let objectKeys = objects.map((metadata) => ({Key: metadata.Key}))
    let response = await s3.deleteObjects({Bucket: bucketName, Delete: {Objects: objectKeys}})
    if (response.Errors && response.Errors.length > 0) {
      const msg = response.Errors.forEach(({Code, Key, Message}) => `${Code}, ${Key}:\n${Message}`).join('\n')
      throw new Error(msg)
    }
  }
}

exports.handler = async function handler(event, context) {
  console.log("REQUEST RECEIVED:\n" + JSON.stringify(event));
  try {
    switch (event.RequestType) {
      case 'Create':
        await response.send(event, context, response.SUCCESS, {})
        break;
      case 'Update':
        await response.send(event, context, response.SUCCESS, {})
        break;
      case 'Delete':
        try {
          await emptyBucket(event.ResourceProperties.BucketToEmpty);
          await response.send(event, context, response.SUCCESS, {})
        } catch (error) {
          await response.send(event, context, response.FAILED, {error: error.message})
        }
        break;
      default:
        await response.send(event, context, response.FAILED, {error: 'Unknown RequestType'})
    }
  }
  catch (error) {
    console.log(error.message)
  }
}
