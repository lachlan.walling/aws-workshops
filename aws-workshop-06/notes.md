# Notes
- Automatically trigger pipeline on commit
- Region
- move templates to a folder
- do we need cloudformation output json
- share empty bucket lambda between pipelines
- rename pipeline.yml - deployment-infrastructure?

custom resource to clear bucket
cloudformation Configuration Property RoleArn should not be empty in current ActionMode

- run frontend cloudformation as developer?
- limit pipeline role to only access created cloudformation stack
- cleanup frontend stack action
- hardcoded repo name, cross stack

- Can we create a policy in frontend to give lambda access to specific bucket